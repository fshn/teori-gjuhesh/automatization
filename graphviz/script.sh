#!/bin/sh
set -ex
cd $(dirname $0)

dot -Tsvg -o epsilon-nfa.svg epsilon-nfa.gv
dot -Tsvg -o nfa.svg nfa.gv
dot -Tsvg -o dfa.svg dfa.gv
dot -Tsvg -o min-dfa.svg min-dfa.gv

dot -Tsvg -o dfa-2.svg dfa-2.gv
dot -Tsvg -o min-dfa-2.svg min-dfa-2.gv

dot -Tsvg -o dfa-3.svg dfa-3.gv
dot -Tsvg -o min-dfa-3.svg min-dfa-3.gv

dot -Tsvg -o dfa-4.svg dfa-4.gv
dot -Tsvg -o min-dfa-4.svg min-dfa-4.gv

dot -Tsvg -o nfa-5.svg nfa-5.gv
dot -Tsvg -o dfa-5.svg dfa-5.gv
dot -Tsvg -o min-dfa-5.svg min-dfa-5.gv

dot -Tsvg -o nfa-6.svg nfa-6.gv
dot -Tsvg -o dfa-6.svg dfa-6.gv
dot -Tsvg -o min-dfa-6.svg min-dfa-6.gv

dot -Tsvg -o epsilon-nfa-7.svg epsilon-nfa-7.gv
dot -Tsvg -o nfa-7.svg nfa-7.gv
dot -Tsvg -o dfa-7.svg dfa-7.gv
dot -Tsvg -o min-dfa-7.svg min-dfa-7.gv
