package algorithm;

import datastructure.Matrix;
import datastructure.MinimizedDfa;

import java.util.*;
import java.util.stream.Collectors;

public class DfaMinimizer {
	private Matrix dfa;

	public DfaMinimizer(Matrix dfa) {
		this.dfa = dfa;
	}

	public void setDfa(Matrix dfa) {
		this.dfa = dfa;
	}

	public Matrix minimize() {
		var size = dfa.states;
		var matrix = generateMarkedMatrix();
		if (markFinalNonFinalStates(matrix, size) == 0) {
			return dfa;
		}

		mark(matrix, size);
		var equivalentStates = sweep(matrix, size);
		return new MinimizedDfa(dfa, equivalentStates);
	}

	// PRIVATE

	private boolean[][] generateMarkedMatrix() {
		boolean matrix[][] = new boolean[dfa.states][];

		for (int i = 0; i < dfa.states; ++i) {
			matrix[i] = new boolean[i];
		}
		return matrix;
	}

	private int markFinalNonFinalStates(boolean matrix[][], int size) {
		var markings = 0;

		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < i; ++j) {
				if ((isFinal(i) && !isFinal(j)) || (!isFinal(i) && isFinal(j))) {
					++markings;
					matrix[i][j] = true;
				}
			}
		}
		return markings;
	}

	private boolean isFinal(int state) {
		return dfa.finalStates.contains(state);
	}

	private void mark(boolean matrix[][], int size) {
		while (true) {
			var markings = 0;
			for (int i = 0; i < size; ++i) {
				for (int j = 0; j < i; ++j) {
					if (!matrix[i][j] && wasMarkedFromPreviousRun(i, j, matrix)) {
						++markings;
						matrix[i][j] = true;
					}
				}
			}
			if (markings == 0) {
				break;
			}
		}
	}

	private boolean wasMarkedFromPreviousRun(int i, int j, boolean matrix[][]) {
		assert i != j;
		for (int k = 0; k < dfa.symbols; ++k) {
			int y = dfa.get(i, k).outState.get(0);
			int x = dfa.get(j, k).outState.get(0);

			if (y == x) {
				continue;
			}
			if (y < x) {
				int tmp = y;
				y = x;
				x = tmp;
			}
			if (matrix[y][x]) {
				return true;
			}
		}
		return false;
	}

	private List<List<Integer>> sweep(boolean matrix[][], int size) {
		var set = new ArrayList<HashSet<Integer>>();

		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < i; ++j) {
				if (!matrix[i][j]) {
					set.add(new HashSet<>());
					var index = set.size() - 1;
					set.get(index).add(i);
					set.get(index).add(j);
				}
			}
		}
		mergeSubsets(set);
		return sortedSubsets(set);
	}

	private void mergeSubsets(ArrayList<HashSet<Integer>> sets) {
		while (true) {
			var swaps = 0;
			for (int i = 0; i < sets.size() - 1; ++i) {
				for (int j = i + 1; j < sets.size(); ++j) {
					var left = sets.get(i);
					var right = sets.get(j);
					if (!intersectionIsEmpty(left, right)) {
						left.addAll(right);
						sets.remove(j--);
						++swaps;
					}
				}
			}
			if (swaps == 0) {
				break;
			}
		}
	}

	private List<List<Integer>> sortedSubsets(ArrayList<HashSet<Integer>> sets) {
		return sets.stream().map(subset -> {
			var list = new ArrayList<Integer>(subset);
			list.sort(Comparator.naturalOrder());
			return list;
		}).collect(Collectors.toList());
	}

	private boolean intersectionIsEmpty(Set<Integer> lhs, Set<Integer> rhs) {
		for (var el : lhs) {
			if (rhs.contains(el)) {
				return false;
			}
		}
		return true;
	}
}
