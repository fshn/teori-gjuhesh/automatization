package algorithm;

import datastructure.Matrix;
import datastructure.TransitionSet;

final class Common {
	private Common() {}

	// PRIVATE

	public static TransitionSet walkWithSymbol(
			Matrix matrix,
			TransitionSet startingStates,
			int symbol
	) {
		var out = new TransitionSet(matrix.states);
		for (int i = 0; i < startingStates.size(); ++i) {
			out.add(matrix.get(startingStates.outState.get(i), symbol));
		}
		return out;
	}
}
