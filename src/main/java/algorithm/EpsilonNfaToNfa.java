package algorithm;

import datastructure.Matrix;
import datastructure.TransitionSet;

public final class EpsilonNfaToNfa {
	private Matrix epsilonNfa;

	public EpsilonNfaToNfa(Matrix epsilonNfa) {
		this.epsilonNfa = epsilonNfa;
	}

	public void setEpsilonNfa(Matrix epsilonNfa) {
		this.epsilonNfa = epsilonNfa;
	}

	public Matrix convert() {
		// all the symbols will be shifted by -1, because \epsilon will be eliminated
		var matrix = new Matrix(epsilonNfa.states, epsilonNfa.symbols - 1);
		matrix.finalStates = epsilonNfa.finalStates;

		for (int i = 0; i < epsilonNfa.states; ++i) {
			var epsilonTransition1 = walkEpsilonStar(i);
			for (int j = 1; j < epsilonNfa.symbols; ++j) {
				var transitionsSymbolX = Common.walkWithSymbol(epsilonNfa, epsilonTransition1, j);
				for (int k = 0; k < transitionsSymbolX.size(); ++k) {
					var epsilonTransition2 = walkEpsilonStar(transitionsSymbolX.outState.get(k));
					for (int l = 0; l < epsilonTransition2.size(); ++l) {
						var endState = epsilonTransition2.outState.get(l);
						matrix.addTransition(i, j - 1, endState);
					}
				}
			}
		}
		return matrix;
	}

	// PRIVATE

	/**
	 * Perform a walk with \epsilon^{\ast} starting from state <code>state</code>
	 */
	public TransitionSet walkEpsilonStar(int state) {
		var out = new TransitionSet(epsilonNfa.states);
		out.outState.add(state);
		__walkEpsilonStar(state, out);
		return out;
	}

	private void __walkEpsilonStar(int state, TransitionSet acc) {
		var state_x_with_epsilon = epsilonNfa.get(state, 0);
		for (int i = 0; i < state_x_with_epsilon.size(); ++i) {
			var transition = state_x_with_epsilon.outState.get(i);
			acc.transitTo(transition);
			__walkEpsilonStar(transition, acc);
		}
	}

}
