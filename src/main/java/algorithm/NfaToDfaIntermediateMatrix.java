package algorithm;

import datastructure.Matrix;
import datastructure.TransitionSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

class NfaToDfaIntermediateMatrix {
	public final int symbols;
	public final int originalNfaStates;
	public final HashSet<Integer> originalFinalStates;

	private final HashSet<TransitionSet> dfaFinalStates;
	private final HashMap<TransitionSet, ArrayList<TransitionSet>> transitionsForEachState;

	public NfaToDfaIntermediateMatrix(Matrix nfa) {
		this.symbols = nfa.symbols;
		this.originalNfaStates = nfa.states;
		this.originalFinalStates = nfa.finalStates;
		this.dfaFinalStates = new HashSet<>();
		this.transitionsForEachState = new HashMap<>();
	}

	public void addState(TransitionSet state) {
		if (state.containsAnyState(originalFinalStates)) {
			dfaFinalStates.add(state);
		}
		transitionsForEachState.put(state, new ArrayList<>(originalNfaStates));
	}

	public boolean contains(TransitionSet transitionSet) {
		return transitionsForEachState.containsKey(transitionSet);
	}

	public void addTransition(TransitionSet start, TransitionSet end) {
		var state = transitionsForEachState.get(start);
		assert state.size() < originalNfaStates;
		state.add(end);
	}

	public Matrix toDfa() {
		var dfa = new Matrix(transitionsForEachState.size(), symbols);
		var list = new ArrayList<>(transitionsForEachState.keySet());
		list.sort(TransitionSet::compareTo);

		for (int i = 0; i < list.size(); ++i) {
			var transitions = transitionsForEachState.get(list.get(i));
			for (int j = 0; j < symbols; ++j) {
				dfa.addTransition(i, j, list.indexOf(transitions.get(j)));
			}
		}
		dfaFinalStates.forEach(state -> dfa.addFinalState(list.indexOf(state)));
		return dfa;
	}
}
