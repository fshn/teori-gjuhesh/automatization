package algorithm;

import datastructure.Matrix;
import datastructure.TransitionSet;

import java.util.Stack;

public final class NfaToDfa {
	private Matrix nfa;

	public NfaToDfa(Matrix nfa) {
		this.nfa = nfa;
	}

	public void setNfa(Matrix nfa) {
		this.nfa = nfa;
	}

	public Matrix convert() {
		var stack = new Stack<TransitionSet>();
		var intermediateMatrix = new NfaToDfaIntermediateMatrix(nfa);
		var current = new TransitionSet(nfa.states);
		current.transitTo(0);
		stack.push(current);
		intermediateMatrix.addState(current);

		while (!stack.isEmpty()) {
			current = stack.pop();
			for (int i = 0; i < nfa.symbols; ++i) {
				var transition_with_x = new TransitionSet(nfa.states);
				transition_with_x.add(Common.walkWithSymbol(nfa, current, i));
				if (!intermediateMatrix.contains(transition_with_x)) {
					intermediateMatrix.addState(transition_with_x);
					stack.push(transition_with_x);
				}
				intermediateMatrix.addTransition(current, transition_with_x);
			}
		}
		return intermediateMatrix.toDfa();
	}
}
