package os;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public final class GraphGenerator {
	private GraphGenerator() {}

	public static void runGraphviz(String inputFilePath, String outputFileName) {
		try {
			var process = new ProcessBuilder().command("script.sh").start();
			var success = process.waitFor(10, TimeUnit.SECONDS);
			if (!success) {
				throw new RuntimeException("Process did not finish in time");
			}
		}
		catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void testRun() {
		try {
			//			var process = Runtime.getRuntime().exec("ls -alh > test.txt");
			//			var in = process.getInputStream();
			//			new BufferedReader(new InputStreamReader(in)).lines().forEach(System.out::println);
			//
			//			if (!process.waitFor(10, TimeUnit.SECONDS)) {
			//				throw new RuntimeException("Process did not finish in time");
			//			}

			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.command("bash", "-c", "ls -alh");
			Process process = processBuilder.start();

			StringBuilder output = new StringBuilder();

			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line).append("\n");
			}

			int exitVal = process.waitFor();
			if (exitVal == 0) {
				System.out.println("Success!");
				System.out.println(output);
				System.exit(0);
			}
		}
		catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}
