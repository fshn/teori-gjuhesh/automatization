import algorithm.DfaMinimizer;
import algorithm.EpsilonNfaToNfa;
import algorithm.NfaToDfa;
import io.GraphvizWriter;
import io.InputReader;

public class Main {
	static String GRAPHVIZ_DIR = "graphviz/";

	public static void main(String[] args) {
		var matrix = InputReader.parseFile("input-7.csv");
		var writer = GraphvizWriter.init();
		writer.setMatrix(matrix);
		writer.write(GRAPHVIZ_DIR + "epsilon-nfa-7.gv");

		var epsilonNfaToNfa = new EpsilonNfaToNfa(matrix);
		var nfa = epsilonNfaToNfa.convert();
		writer.setMatrix(nfa);
		writer.write(GRAPHVIZ_DIR + "nfa-7.gv");

		var nfaToDfa = new NfaToDfa(nfa);
		var dfa = nfaToDfa.convert();
		writer.setMatrix(dfa);
		writer.write(GRAPHVIZ_DIR + "dfa-7.gv");

		var dfaMinimizer = new DfaMinimizer(dfa);
		var minimizedDfa = dfaMinimizer.minimize();
		writer.setMatrix(minimizedDfa);
		writer.write(GRAPHVIZ_DIR + "min-dfa-7.gv");
	}
}
