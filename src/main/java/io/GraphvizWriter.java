package io;

import datastructure.Matrix;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.Arrays;

public class GraphvizWriter {
	private static final String OPENING_LINE = "digraph finite_state_machine {";
	private static final String HEADER[] = {
			"rankdir=LR", "size=10"
	};
	private static final String SIMPLE_CIRCLES = "node [shape=circle];";
	private static final String LAST_LINE = "}";
	private Matrix matrix;

	private GraphvizWriter() {}

	public static GraphvizWriter init() {
		return new GraphvizWriter();
	}

	public void setMatrix(Matrix matrix) {
		this.matrix = matrix;
	}

	// PRIVATE

	public void write(String pathToFile) {
		var path = Path.of(pathToFile);
		try (var stdout = new PrintWriter(new BufferedWriter(new FileWriter(path.toFile())))) {
			stdout.println(OPENING_LINE);
			printMetadata(stdout);
			printMatrix(stdout);
			stdout.println(LAST_LINE);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void printMetadata(PrintWriter stdout) {
		Arrays.stream(HEADER).forEach(el -> stdout.printf("\t%s;\n", el));
		stdout.printf("\tnode [shape = doublecircle];");
		matrix.finalStates.forEach(el -> stdout.printf(" %d", el));
		stdout.println(";");
		stdout.printf("\t%s\n", SIMPLE_CIRCLES);
	}

	private void printMatrix(PrintWriter stdout) {
		for (int i = 0; i < matrix.states; ++i) {
			for (int j = 0; j < matrix.transitionsForEachState.get(i).size(); ++j) {
				var transitions = matrix.get(i, j).outState;
				for (var el : transitions) {
					stdout.printf("\t%d -> %d [label = \"%d\"];\n", i, el, j);
				}
			}
		}
	}
}
