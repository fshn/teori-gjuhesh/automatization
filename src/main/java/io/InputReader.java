package io;

import datastructure.Matrix;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

public class InputReader {
	public static final String DEFAULT_PATH = "input-1.csv";
	private static final String DELIMITER = ",";

	private InputReader() {}

	public static Matrix parseFile(String path) {
		Matrix matrix = null;
		var filesystemPath = Path.of(path);
		try (var stdin = new Scanner(new BufferedReader(new FileReader(filesystemPath.toFile())))) {
			// memory allocation
			var metadata = stdin.nextLine().split(DELIMITER);
			assert metadata.length == 2;
			matrix = new Matrix(Integer.parseInt(metadata[0]), Integer.parseInt(metadata[1]));

			// final states
			Arrays.stream(stdin.nextLine().split(DELIMITER))
					.mapToInt(Integer::parseInt)
					.forEach(matrix::addFinalState);

			// transitions
			while (stdin.hasNext()) {
				var delta = stdin.nextLine().split(DELIMITER);
				assert delta.length == 3;
				var start = Integer.parseInt(delta[0]);
				var symbol = Integer.parseInt(delta[1]);
				var end = Integer.parseInt(delta[2]);
				matrix.addTransition(start, symbol, end);
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return matrix;
	}
}
