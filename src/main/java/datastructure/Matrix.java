package datastructure;

import java.util.ArrayList;
import java.util.HashSet;

public sealed class Matrix permits MinimizedDfa {
	public final int states;
	public final int symbols;
	public HashSet<Integer> finalStates;
	/**
	 * Bashkesia e gjendjeve me tranzicionet e seciles cdo index perfaqeson nje gjendje.
	 */
	public ArrayList<ArrayList<TransitionSet>> transitionsForEachState;

	public Matrix(int states, int symbols) {
		this.states = states;
		this.symbols = symbols;
		this.finalStates = new HashSet<>();
		this.transitionsForEachState = new ArrayList<>(states);

		for (int i = 0; i < states; ++i) {
			transitionsForEachState.add(new ArrayList<>(symbols));
			for (int j = 0; j < symbols; ++j) {
				transitionsForEachState.get(i).add(new TransitionSet(states));
			}
		}
	}

	public void addFinalState(int state) {
		finalStates.add(state);
	}

	/**
	 * Krijo nje tranzicion nga <code>start</code> ne <code>end</code> permes simbolit
	 * <code>symbol</code>.
	 * <b>Shenim:</b> <code>0</code> konsiderohet si \epsilon.
	 */
	public void addTransition(int start, int symbol, int end) {
		transitionsForEachState.get(start).get(symbol).transitTo(end);
	}

	/**
	 * @return listen me gjendjet ku gjendja <code>state</code> perfundon pasi merr inputin
	 * <code>symbol</code>
	 */
	public TransitionSet get(int state, int symbol) {
		return transitionsForEachState.get(state).get(symbol);
	}
}
