package datastructure;


import java.util.HashSet;
import java.util.List;

public final class MinimizedDfa extends Matrix {
	private final int[] renamedStates;

	@Deprecated
	public MinimizedDfa(int states, int symbols) {
		super(states, symbols);
		renamedStates = new int[states];
		for (int i = 0; i < states; ++i) {
			renamedStates[i] = i;
		}
	}

	public MinimizedDfa(Matrix original, List<List<Integer>> equivalentStates) {
		this(original.states, original.symbols);
		//		int rename = equivalentStates.iterator().next();
		//		original.finalStates.stream()
		//				.map(el -> {
		//					if (equivalentStates.contains(el)) {
		//						return rename;
		//					}
		//					return el;
		//				})
		//				.forEach(this::addFinalState);
		//		set(original);
		//		rename(equivalentStates);
		initFinalStates(original, equivalentStates);
		copyOldMatrix(original);
		rename_remove_states_in_equivalent_set(equivalentStates);
	}

	private static int indexIn(int el, List<List<Integer>> sets) {
		for (int i = 0; i < sets.size(); ++i) {
			if (sets.get(i).contains(el)) {
				return i;
			}
		}
		return -1;
	}

	private static int indexInRange(int el, int list[], int start, int end) {
		for (int i = start; i < end; ++i) {
			if (list[i] == el) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public TransitionSet get(int state, int symbol) {
		return super.get(renamedStates[state], symbol);
	}

	public TransitionSet oldGet(int state, int symbol) {
		return transitionsForEachState.get(state).get(symbol);
	}

	// PRIVATE

	public void renameState(int oldName, int newName) {
		renamedStates[oldName] = newName;
	}

	public void renameTransitionState(int oldStartState, int symbol, int newName) {
		transitionsForEachState.get(oldStartState).get(symbol).outState.set(0, newName);
	}

	private void initFinalStates(Matrix original, List<List<Integer>> equivalentStates) {
		for (var el : original.finalStates) {
			int name = el;
			int index = indexIn(el, equivalentStates);
			if (index != -1) {
				name = equivalentStates.get(index).iterator().next();
			}
			addFinalState(name);
		}
	}

	private void rename(HashSet<Integer> equivalentStates) {
		int renameTo = equivalentStates.iterator().next();
		for (int i = 0; i < states; ++i) {
			if (equivalentStates.contains(i)) {
				renameState(i, renameTo);
			}
			for (int j = 0; j < symbols; ++j) {
				if (equivalentStates.contains(oldGet(i, j).outState.get(0))) {
					renameTransitionState(i, j, renameTo);
				}
			}
		}
	}

	private void copyOldMatrix(Matrix original) {
		for (int i = 0; i < original.states; ++i) {
			for (int j = 0; j < original.symbols; ++j) {
				var transitions = original.get(i, j).outState;
				for (var state : transitions) {
					addTransition(i, j, state);
				}
			}
		}
	}

	private void rename_remove_states_in_equivalent_set(List<List<Integer>> equivalentStates) {
		for (int i = 0; i < states; ++i) {
			int renamedStateIndex = indexIn(i, equivalentStates);
			if (renamedStateIndex != -1) {
				int rename = equivalentStates.get(renamedStateIndex).iterator().next();
				renameState(i, rename);
				int pastOccurrence = indexInRange(rename, renamedStates, 0, i);
				if (pastOccurrence != -1) {
					transitionsForEachState.get(i).clear();
					continue;
				}
			}
			for (int j = 0; j < symbols; ++j) {
				int state = oldGet(i, j).outState.get(0);
				int renamedTransitionStateIndex = indexIn(state, equivalentStates);
				if (renamedTransitionStateIndex != -1) {
					renameTransitionState(
							i,
							j,
							equivalentStates.get(renamedTransitionStateIndex).iterator().next()
					);
				}
			}
		}
	}
}
