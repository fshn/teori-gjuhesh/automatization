package datastructure;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

public class TransitionSet implements Comparable<TransitionSet> {
	// bashkesia e gjendjeve output pasi aplikohet funksioni i kalimit
	public ArrayList<Integer> outState;

	public TransitionSet(int states) {
		this.outState = new ArrayList<>(states);
	}

	public void transitTo(int state) {
		if (!outState.contains(state)) {
			outState.add(state);
			outState.sort(Comparator.naturalOrder());
		}
	}

	public void add(TransitionSet transitions) {
		transitions.outState.forEach(this::transitTo);
	}

	public boolean isEmpty() {
		return outState.isEmpty();
	}

	public int size() {
		return outState.size();
	}

	public boolean containsAnyState(HashSet<Integer> states) {
		for (var state : states) {
			if (outState.contains(state)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return outState.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o instanceof TransitionSet set) {
			return outState.equals(set.outState);
		}
		if (o instanceof List<?>) {
			return outState.equals(o);
		}
		return false;
	}

	@Override
	public String toString() {
		return "TransitionSet{" + "outState=" + outState + '}';
	}

	@Override
	public int compareTo(TransitionSet o) {
		if (o.isEmpty()) {
			return -1;
		}
		var iter = Math.min(this.outState.size(), o.outState.size());

		for (int i = 0; i < iter; ++i) {
			int lhs = outState.get(i);
			int rhs = o.outState.get(i);
			if (lhs != rhs) {
				return Integer.compare(lhs, rhs);
			}
		}

		if (iter == outState.size()) {
			return -1;
		}
		return 1;
	}
}
