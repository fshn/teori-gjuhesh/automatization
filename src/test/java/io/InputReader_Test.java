package io;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InputReader_Test {
	@Test
	@DisplayName("Loads correctly")
	void test1() {
		var matrix = InputReader.parseFile(InputReader.DEFAULT_PATH);
		var states = 6;
		var symbols = 3;

		assertEquals(matrix.states, states);
		assertEquals(matrix.symbols, symbols);
		assertEquals(matrix.finalStates, Set.of(5));

		var expectedMatrix = List.of(
				// state 0
				List.of(
						// symbol 0
						List.of(1, 3), List.of(0), List.of(0)),
				// state 1
				List.of(List.of(), List.of(2), List.of()),
				// state 2
				List.of(List.of(5), List.of(), List.of()),
				// state 3
				List.of(List.of(), List.of(), List.of(4)),
				// state 4
				List.of(List.of(5), List.of(), List.of()),
				// state 5
				List.of(List.of(), List.of(5), List.of(5))
		);
		for (int i = 0; i < states; ++i) {
			for (int j = 0; j < symbols; ++j) {
				// ignore warning
				// TransitionSet is all in all an alias for ArrayList<Integer>
				assertEquals(matrix.get(i, j), expectedMatrix.get(i).get(j));
			}
		}
	}
}
