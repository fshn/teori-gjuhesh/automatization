import algorithm.DfaMinimizer;
import algorithm.EpsilonNfaToNfa;
import algorithm.NfaToDfa;
import io.GraphvizWriter;
import io.InputReader;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestAll {
	static String EPSILON_NFA = "epsilon-nfa.dump.gv";
	static String NFA = "nfa.dump.gv";
	static String DFA = "dfa.dump.gv";
	static String MIN_DFA = "min-dfa.dump.gv";
	static String GRAPHVIZ_DUMP_DIR = "src/test/java/graphviz/";


	@Test
	void test1() {
		var matrix = InputReader.parseFile(InputReader.DEFAULT_PATH);
		var writer = GraphvizWriter.init();
		writer.setMatrix(matrix);
		writer.write(GRAPHVIZ_DUMP_DIR + EPSILON_NFA);
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + EPSILON_NFA),
					Path.of(GRAPHVIZ_DUMP_DIR + "epsilon-nfa.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var epsilonNfaToNfa = new EpsilonNfaToNfa(matrix);
		var nfa = epsilonNfaToNfa.convert();
		writer.setMatrix(nfa);
		writer.write(GRAPHVIZ_DUMP_DIR + NFA);
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + NFA),
					Path.of(GRAPHVIZ_DUMP_DIR + "nfa.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var nfaToDfa = new NfaToDfa(nfa);
		var dfa = nfaToDfa.convert();
		writer.setMatrix(dfa);
		writer.write(GRAPHVIZ_DUMP_DIR + DFA);
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + DFA),
					Path.of(GRAPHVIZ_DUMP_DIR + "dfa.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var dfaMinimizer = new DfaMinimizer(dfa);
		var minimizedDfa = dfaMinimizer.minimize();
		writer.setMatrix(minimizedDfa);
		writer.write(GRAPHVIZ_DUMP_DIR + MIN_DFA);
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + MIN_DFA),
					Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void test2() {
		var matrix = InputReader.parseFile("input-2.csv");
		var writer = GraphvizWriter.init();
		writer.setMatrix(matrix);
		writer.write(GRAPHVIZ_DUMP_DIR + "dfa-2.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "dfa-2.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "dfa-2.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var dfaMinimizer = new DfaMinimizer(matrix);
		var minDfa = dfaMinimizer.minimize();
		writer.setMatrix(minDfa);
		writer.write(GRAPHVIZ_DUMP_DIR + "min-dfa-2.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-2.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-2.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void test3() {
		var matrix = InputReader.parseFile("input-3.csv");
		var writer = GraphvizWriter.init();
		writer.setMatrix(matrix);
		writer.write(GRAPHVIZ_DUMP_DIR + "dfa-3.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "dfa-3.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "dfa-3.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var dfaMinimizer = new DfaMinimizer(matrix);
		var minDfa = dfaMinimizer.minimize();
		writer.setMatrix(minDfa);
		writer.write(GRAPHVIZ_DUMP_DIR + "min-dfa-3.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-3.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-3.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void test4() {
		var matrix = InputReader.parseFile("input-4.csv");
		var writer = GraphvizWriter.init();
		writer.setMatrix(matrix);
		writer.write(GRAPHVIZ_DUMP_DIR + "dfa-4.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "dfa-4.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "dfa-4.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var dfaMinimizer = new DfaMinimizer(matrix);
		var minDfa = dfaMinimizer.minimize();
		writer.setMatrix(minDfa);
		writer.write(GRAPHVIZ_DUMP_DIR + "min-dfa-4.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-4.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-4.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void test5() {
		var matrix = InputReader.parseFile("input-5.csv");
		var writer = GraphvizWriter.init();
		writer.setMatrix(matrix);
		writer.write(GRAPHVIZ_DUMP_DIR + "nfa-5.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "nfa-5.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "nfa-5.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var nfaToDfa = new NfaToDfa(matrix);
		var dfa = nfaToDfa.convert();
		writer.setMatrix(dfa);
		writer.write(GRAPHVIZ_DUMP_DIR + "dfa-5.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "dfa-5.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "dfa-5.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var dfaMinimizer = new DfaMinimizer(dfa);
		var minDfa = dfaMinimizer.minimize();
		writer.setMatrix(minDfa);
		writer.write(GRAPHVIZ_DUMP_DIR + "min-dfa-5.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-5.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-5.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void test6() {
		var matrix = InputReader.parseFile("input-6.csv");
		var writer = GraphvizWriter.init();
		writer.setMatrix(matrix);
		writer.write(GRAPHVIZ_DUMP_DIR + "nfa-6.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "nfa-6.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "nfa-6.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var nfaToDfa = new NfaToDfa(matrix);
		var dfa = nfaToDfa.convert();
		writer.setMatrix(dfa);
		writer.write(GRAPHVIZ_DUMP_DIR + "dfa-6.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "dfa-6.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "dfa-6.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}

		var dfaMinimizer = new DfaMinimizer(dfa);
		var minDfa = dfaMinimizer.minimize();
		writer.setMatrix(minDfa);
		writer.write(GRAPHVIZ_DUMP_DIR + "min-dfa-6.dump.gv");
		try {
			assertEquals(Files.mismatch(Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-6.dump.gv"),
					Path.of(GRAPHVIZ_DUMP_DIR + "min-dfa-6.gv")
			), -1);
		}
		catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}
}