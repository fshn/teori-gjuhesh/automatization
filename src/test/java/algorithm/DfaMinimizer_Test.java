package algorithm;

import io.InputReader;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DfaMinimizer_Test {
	@Test
	@DisplayName("Convertion from NFA to DFA works")
	void t1() {
		var epsilonNfaToNfa = new EpsilonNfaToNfa(InputReader.parseFile(InputReader.DEFAULT_PATH));
		var nfa = epsilonNfaToNfa.convert();
		var nfaToDfa = new NfaToDfa(nfa);
		var dfa = nfaToDfa.convert();
		var dfaMinimizer = new DfaMinimizer(dfa);
		var matrix = dfaMinimizer.minimize();
		var expectedMatrix = List.of(
				// state 0
				List.of(List.of(1), List.of(1)),
				// state 1
				List.of(List.of(1), List.of(1)),
				// state 2
				/// HACK
				/// this is a hack because in reality list was cleared
				List.of(List.of(1), List.of(1))
		);
		var states = dfa.states;
		var symbols = dfa.symbols;

		assertEquals(matrix.states, states);
		assertEquals(matrix.symbols, symbols);
		for (int i = 0; i < states; ++i) {
			for (int j = 0; j < symbols; ++j) {
				// ignore warning
				// TransitionSet is all in all an alias for ArrayList<Integer>
				var lhs = matrix.get(i, j);
				var rhs = expectedMatrix.get(i).get(j);
				assertEquals(lhs, rhs);
			}
		}
	}
}
