package algorithm;

import io.InputReader;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NfaToDfa_Test {
	@Test
	@DisplayName("Convertion from NFA to DFA works")
	void t1() {
		var epsilonNfaToNfa = new EpsilonNfaToNfa(InputReader.parseFile(InputReader.DEFAULT_PATH));
		var nfa = epsilonNfaToNfa.convert();
		var nfaToDfa = new NfaToDfa(nfa);
		var matrix = nfaToDfa.convert();
		var expectedMatrix = List.of(
				// state 0
				List.of(List.of(1), List.of(2)),
				// state 1
				List.of(List.of(1), List.of(2)),
				// state 2
				List.of(List.of(1), List.of(2))
		);
		var states = expectedMatrix.size();
		var symbols = expectedMatrix.get(0).size();

		assertEquals(matrix.states, states);
		assertEquals(matrix.symbols, symbols);
		for (int i = 0; i < states; ++i) {
			for (int j = 0; j < symbols; ++j) {
				// ignore warning
				// TransitionSet is all in all an alias for ArrayList<Integer>
				assertEquals(matrix.get(i, j), expectedMatrix.get(i).get(j));
			}
		}
	}
}
