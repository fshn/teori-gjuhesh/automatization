package algorithm;

import io.InputReader;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EpsilonNfaToNfa_Test {
	@Test
	@DisplayName("Convertion from \\epsilon-NFA to NFA works")
	void t1() {
		var converter = new EpsilonNfaToNfa(InputReader.parseFile(InputReader.DEFAULT_PATH));
		var matrix = converter.convert();
		var expectedMatrix = List.of(
				// state 0
				List.of(List.of(0, 1, 2, 3, 5), List.of(0, 1, 3, 4, 5)),
				// state 1
				List.of(List.of(2, 5), List.of()),
				// state 2
				List.of(List.of(5), List.of(5)),
				// state 3
				List.of(List.of(), List.of(4, 5)),
				// state 4
				List.of(List.of(5), List.of(5)),
				// state 5
				List.of(List.of(5), List.of(5))
		);
		var states = expectedMatrix.size();
		var symbols = expectedMatrix.get(0).size();

		assertEquals(matrix.states, states);
		assertEquals(matrix.symbols, symbols);
		for (int i = 0; i < states; ++i) {
			for (int j = 0; j < symbols; ++j) {
				// ignore warning
				// TransitionSet is all in all an alias for ArrayList<Integer>
				assertEquals(matrix.get(i, j), expectedMatrix.get(i).get(j));
			}
		}
	}
}
